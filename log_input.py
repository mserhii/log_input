def read_data_from_file_into_curcor(csv_file) :
    with open(csv_file, newline='') as csvfilecursor:
        next(csvfilecursor, None).split(";")
        #print(type(csvfilecursor))

        user_times = {}
        connected_users = {}
        for line in csvfilecursor:
            user = line.split(";")[1]
#            print(user)

            if line.split(";")[3] == "CONNECT" and not connected_users.__contains__(user):
                connected_users[user] = line.split(";")[0]
 #               print(connected_users)
            elif line.split(";")[3] == "DISCONNECT" and connected_users.__contains__(user):
                h, m, s = connected_users[user].split(':')
                user_started_seconds = int(h) * 3600 + int(m) * 60 + int(s)

#                print(user_started_seconds)

                h, m, s = line.split(";")[0].split(':')
                user_finished_seconds = int(h) * 3600 + int(m) * 60 + int(s)

                connection_time = user_finished_seconds - user_started_seconds

                if not user_times.__contains__(user):
                    user_times[user] = connection_time
                else:
                    user_times[user] += connection_time

                # remove the user from connected_userd dict
                connected_users.pop(user, None)

        print(user_times)



read_data_from_file_into_curcor("log_input.csv")